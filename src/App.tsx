import { ComputerBox } from "components/computerBox";
import { Footer } from "components/footer";
import { Action } from "model";
import { useCallback, useState } from "react";
import "./App.css";
import { Header } from "./components/header";
import { PlayerBox } from "./components/playerBox";

function computerActionChoice() {
  const actions = [Action.Rock, Action.Paper, Action.Scissors];

  const randomIndex = Math.floor(Math.random() * actions.length);

  return actions[randomIndex];
}

function decideScore(action1: Action, action2: Action): number {
  if (action1 === action2) {
    return 0;
  }

  if (action1 === Action.Rock) {
    if (action2 === Action.Paper) {
      return -1;
    } else {
      return 1;
    }
  }
  if (action1 === Action.Paper) {
    if (action2 === Action.Scissors) {
      return -1;
    } else {
      return 1;
    }
  } else if (action1 === Action.Scissors) {
    if (action2 === Action.Rock) {
      return -1;
    } else {
      return 1;
    }
  }
  return 0;
}

function App() {
  const [, setPlayerAction] = useState<Action>(Action.Undecided);
  const [computerAction, setComputerAction] = useState<Action>(
    Action.Undecided
  );

  const [playerScore, setPlayerScore] = useState<number>(0);
  const [computerScore, setComputerScore] = useState<number>(0);
  const [matchText, setMatchText] = useState<string>(
    "Please select a choice to begin the match"
  );

  const onPlayerActionSelection = useCallback(
    (selectedAction: Action) => {
      setPlayerAction(selectedAction);

      const computerRandomChoice = computerActionChoice();
      setComputerAction(() => computerRandomChoice);
      const scoreEvolution = decideScore(selectedAction, computerRandomChoice);

      if (scoreEvolution > 0) {
        setPlayerScore((playerScore) => ++playerScore);
        setMatchText(() => "It's a win !");
      } else if (scoreEvolution < 0) {
        setComputerScore((computerScore) => ++computerScore);
        setMatchText(() => "Too bad you lose...");
      } else {
        setMatchText(() => "That's a draw.");
      }
    },
    [setPlayerAction, setComputerAction, setPlayerScore, setComputerScore]
  );

  return (
    <div className="centerContainer">
      <Header />
      <div>
        <ComputerBox
          name={"Computer"}
          action={computerAction}
          score={computerScore}
        />

        <h2>{matchText}</h2>

        <PlayerBox
          name="Human"
          score={playerScore}
          onPlayerActionSelected={onPlayerActionSelection}
        />
      </div>
      <Footer />
    </div>
  );
}

export default App;
