import { Action } from "model";
import { PlayerActionButton } from "./player-action-button";

export function PlayerBoxComponent({
  name,
  score,
  onPlayerActionSelected,
}: {
  name: string;
  score: number;
  onPlayerActionSelected: (selectedAction: Action) => void;
}): JSX.Element {
  return (
    <div className="playerBox">
      <div className={"actionsContainer"}>
        <PlayerActionButton
          action={Action.Rock}
          onPlayerActionSelected={onPlayerActionSelected}
        />
        <PlayerActionButton
          action={Action.Paper}
          onPlayerActionSelected={onPlayerActionSelected}
        />
        <PlayerActionButton
          action={Action.Scissors}
          onPlayerActionSelected={onPlayerActionSelected}
        />
      </div>
      <h2>
        {name}: {score}
      </h2>
    </div>
  );
}

export const PlayerBox = PlayerBoxComponent;
