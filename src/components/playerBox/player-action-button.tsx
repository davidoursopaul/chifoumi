import { Action } from "model";
import rock from "assets/rock.svg";
import paper from "assets/leaf.svg";
import scissors from "assets/scissors.svg";

export function PlayerActionButtonComponent({
  action,
  onPlayerActionSelected,
}: {
  action?: Action;
  onPlayerActionSelected?: (selectedAction: Action) => void;
}): JSX.Element {
  function displayActionSVG(action?: Action) {
    if (!!onPlayerActionSelected) {
      switch (action) {
        case Action.Rock: {
          return (
            <img
              className="actionImg"
              src={rock}
              alt="SVG rock"
              onClick={() => onPlayerActionSelected(action)}
            />
          );
        }
        case Action.Paper: {
          return (
            <img
              className="actionImg"
              src={paper}
              alt="SVG paper"
              onClick={() => onPlayerActionSelected(action)}
            />
          );
        }
        case Action.Scissors: {
          return (
            <img
              className="actionImg"
              src={scissors}
              alt="SVG scissors"
              onClick={() => onPlayerActionSelected(action)}
            />
          );
        }

        default:
          return;
      }
    }
    return;
  }

  return <div className="actionButton">{displayActionSVG(action)}</div>;
}

export const PlayerActionButton = PlayerActionButtonComponent;
