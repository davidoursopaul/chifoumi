export function HeaderComponent(): JSX.Element {
  return (
    <header>
      <h1>Chifoumi</h1>
    </header>
  );
}

export const Header = HeaderComponent;
