import { Action } from "model";
import { ComputerActionButton } from "./computer-action-button";

export function ComputerBoxComponent({
  name,
  action,
  score,
}: {
  name: string;
  action: Action;
  score: number;
}): JSX.Element {
  return (
    <div className="playerBox">
      <div className={"actionsContainer"}>
        <ComputerActionButton action={action} />
      </div>
      <h2>
        {name}: {score}
      </h2>
    </div>
  );
}

export const ComputerBox = ComputerBoxComponent;
