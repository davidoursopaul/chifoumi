import { Action } from "model";
import rock from "assets/rock.svg";
import paper from "assets/leaf.svg";
import scissors from "assets/scissors.svg";

export function ComputerActionButtonComponent({
  action,
}: {
  action?: Action;
}): JSX.Element {
  function displayActionSVG(action?: Action) {
    switch (action) {
      case Action.Rock: {
        return <img className="actionImg" src={rock} alt="SVG rock" />;
      }
      case Action.Paper: {
        return <img className="actionImg" src={paper} alt="SVG paper" />;
      }
      case Action.Scissors: {
        return <img className="actionImg" src={scissors} alt="SVG scissors" />;
      }

      default:
    }
  }

  return <div className="actionButton">{displayActionSVG(action)}</div>;
}

export const ComputerActionButton = ComputerActionButtonComponent;
