export function FooterComponent(): JSX.Element {
  return <footer>Made by Paul DAVID</footer>;
}

export const Footer = FooterComponent;
